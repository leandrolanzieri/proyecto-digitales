/* Proyecto Domotica | Raspberry
 * nodo.js
 * Clase base de la representacion de los nodos de la Red
 * Esta clase sera extendida por clases mas especificas para
 * cada tipo de nodo
 */ 
 
/*******************************************************************************
 * Copyright (c) 2015  
 * Martin Enriquez (martin.hernan.enriquez@gmail.com),
 * Leandro Lanzieri Rodriguez (lanzierileandro@gmail.com).
 *
 * This file is part of Proyecto Domotica.
 *
 *     Proyecto Domotica is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Proyecto Domotica is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Proyecto Domotica.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/*******************************************************************************
 *******************************************************************************/
var util = require("util");
var EventEmitter = require("events").EventEmitter;
var comandos = require("./comunicacion.json");

// Constructor de la clase Nodo
function Nodo (nodo) {
    EventEmitter.call(this);
    this.id = nodo.id;
    this.nombre = nodo.nombre;
    this.frecuencia = nodo.frecuencia;
    this.tipo = nodo.tipo;
    this.estado = nodo.estado;
    this.conectado = nodo.conectado;
}

Nodo.prototype.ping = function () {
	console.log("\nHaciendo  ping a " + this.nombre + " comando: " + comandos.PING);
};

// Imprime en consola las propiedades basicas del nodo
Nodo.prototype.propiedades = function() {
	console.log("-----------------------");
	console.log("\nPropiedades del Nodo");
	console.log("\nId: " + this.id);
	console.log("\nNombre: " + this.nombre);
	console.log("\nFrecuencia: " + this.frecuencia);
	console.log("\nTipo: " + this.tipo);
}

util.inherits(Nodo, EventEmitter);
module.exports = Nodo;