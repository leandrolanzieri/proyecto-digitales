/* Proyecto Domotica | Raspberry
 * nodo-onoff.js
 * Definicion de propiedades y metodos de la clase Nodo OnOff.
 * Esta clase hereda los metodos y propiedades basicas de Nodo.
 */
/*******************************************************************************
 * Copyright (c) 2015  
 * Martin Enriquez (martin.hernan.enriquez@gmail.com),
 * Leandro Lanzieri Rodriguez (lanzierileandro@gmail.com).
 *
 * This file is part of Proyecto Domotica.
 *
 *     Proyecto Domotica is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Proyecto Domotica is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Proyecto Domotica.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
var util = require("util");
var Nodo = require('./nodo.js');
var comunicacion = require('./comunicacion');
function NodoTemperatura (nodo) {
   Nodo.call(this, nodo);
   this.temperatura = nodo.temperatura,
   this.propiedades = function () {
	console.log("-----------------------");
	console.log("\nPropiedades del Nodo");
	console.log("\nId: " + this.id);
	console.log("\nNombre: " + this.nombre);
	console.log("\nFrecuencia: " + this.frecuencia);
	console.log("\nTipo: " + this.tipo);
   };
   
   this.encender = function () {
	   console.log("\nEstoy encendido!.");
   };
   
   this.actualizarEstado = function(nodo) {
	   	if(nodo.estado != this.estado) {
			this.estado = nodo.estado;
			console.log("\n" + this.nombre + " cambia su estado a: " + this.estado);
		}
	};
   
   	this.actualizarTemperatura = function() {
   		var comando = new Buffer([comunicacion.MEDIR_TEMP]);
   		return comando;
   	}
}



util.inherits(NodoTemperatura, Nodo);
module.exports = NodoTemperatura;
