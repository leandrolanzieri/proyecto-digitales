var WebSocket = require('ws'); // Para conectarse al servidor websocket
var clone = require('clone'); // Para clonar objetos
var RuleEngine = require('node-rules'); // Para el motor de reglas
var reglas = require('./reglas.json'); // Reglas guardadas
var NodoOnOff = require("./nodo-onoff"); // Clase de nodo on off
var NodoTemperatura = require("./nodo-temperatura"); // Clase de nodo temperatura
var configuracion = require('./configuracion'); // Configuracion del sistema
var comunicacion = require('./comunicacion');
/*COMUNICACION SERIE*/
var serialPortModule = require("serialport"); // Para la comunicacion serie

//La conexion serie es comun para todos los nodos
var conexion = new serialPortModule.SerialPort("/dev/ttyAMA0", {
baudrate: 9600,
stopBits: 1,
parser: serialPortModule.parsers.raw
}, false); 


/* Los bytes para transmitir deben escribirse en este
* buffer y seran enviados por uart en cada ocurrencia
* del intervalo.
*/
bufferTransmision = new Buffer([]);

/* Es el byte que se transmite en cada ocurrencia
* del intervalo.
*/
var data = [];

/* Intervalo para la transmision serie. Si hay
* bytes en el buffer se escribe el de la
* posicion 0.
*/
var intervalo = setInterval(function() {
	if(bufferTransmision.length != 0) {
		data[0] = bufferTransmision[0];
			console.log("\nTransmitiendo" + data);
			conexion.write(data);
	}
}, 20);

/* Conexion al servidor websocket */
//var ws = new WebSocket(configuracion.servidor);


/* Referencia de fecha */
var fecha = new Date();
var hechoAnterior = {};
/* Instancia del motor de reglas */
var R = new RuleEngine();
//R.fromJSON(reglas); // Se registran las reglas guardadas

/* El fact que es inyectado al rules-engine */
var hecho = {
		"tiempo": {
			"hora": fecha.getHours().toString() + ":" + fecha.getMinutes().toString(),
		    "diaMes": fecha.getDate(),
		    "mes": fecha.getMonth(),
		    "diaSemana": fecha.getDay()
		},
		"nodos": []
}
/* Llena el fact con los nodos guardados en la configuracion */
for(i in configuracion.nodos) {
	if(configuracion.nodos[i].tipo == 'temperatura')
		hecho.nodos.push(new NodoTemperatura(configuracion.nodos[i]));
	else
		hecho.nodos.push(new NodoOnOff(configuracion.nodos[i]));
	// TODO Hacer ping para ver si esta en la red
	hecho.nodos[i].propiedades();
}

//Permite establecer la conecxion con el puerto serie	
conexion.open(function() {
	console.log("\nConexion serie establecida.");
	var ws = new WebSocket(configuracion.servidor);
	
	ws.on('open', function() {
		  ws.send(JSON.stringify({destino: "servidor", ping: "Rasp dice Ping!"}));
		  var ping = setInterval(function () {
			ws.send(JSON.stringify({destino: "servidor", ping: "Rasp dice Ping!"}));
		  }, 15000);
	 });
	  
	ws.on('message', function(message) {
		var mensaje = JSON.parse(message);
		if(mensaje.destino == "raspberry") {
			console.log("\nDestino: " + mensaje.destino+ " | Nodo: " + mensaje.nodo + " | Comando: " + mensaje.comando);
			switch(mensaje.nodo) {
			case "temperatura living":
				// No se deberian recibir mensajes de este nodo
				break;
			case "luces living":
				// Se envia mensaje
				comandos = new Buffer([mensaje.comando]); // Comandos a transmitir
				bufferTransmision = Buffer.concat([bufferTransmision, comandos]); // Se concatenan al buffer
				// Se modifica la informacion del nodo local
				if(hecho.nodos.filter(function( obj ) { return obj.nombre == 'luces living';})[0].estado) {
					hecho.nodos.filter(function( obj ) { return obj.nombre == 'luces living';})[0].estado = false;
					ws.send(JSON.stringify({destino: "servidor", nodo: "ventilacion living", estado: ' apagado'}));
				}
				else {
					hecho.nodos.filter(function( obj ) { return obj.nombre == 'luces living';})[0].estado = true;
					ws.send(JSON.stringify({destino: "servidor", nodo: "luces living", estado: ' encendido'}));
				}
				break;
				// Se envia por ws el nodo actualizado
				ws.send(JSON.stringify({destino: "servidor", nodo: "luces living", estado: hecho.nodos.filter(function( obj ) { return obj.nombre == 'luces living';})[0].estado}));
			case "ventilacion living":
				// Se envia mensaje
				comandos = new Buffer([mensaje.comando]); // Comandos a transmitir
				bufferTransmision = Buffer.concat([bufferTransmision, comandos]); // Se concatenan al buffer
				// Se modifica la informacion del nodo local
				if(hecho.nodos.filter(function( obj ) { return obj.nombre == 'ventilacion living';})[0].estado) {
					hecho.nodos.filter(function( obj ) { return obj.nombre == 'ventilacion living';})[0].estado = false;
					ws.send(JSON.stringify({destino: "servidor", nodo: "ventilacion living", estado: ' apagado'}));
				}
				else {
					hecho.nodos.filter(function( obj ) { return obj.nombre == 'ventilacion living';})[0].estado = true;
					ws.send(JSON.stringify({destino: "servidor", nodo: "ventilacion living", estado: ' encendido'}));
				}
				break;
				// Se envia por ws el nodo actualizado

			}
	  	}
	  });
	  ws.on('close', function () {
		setInterval(function () {
			conectar();
		console.log("Intentando reconexion...");
		}, 5000);
	  });
	
	conexion.on('data', function(mensaje) {
		console.log("\nSe recibe: " + mensaje);
		//if(mensaje == configuracion.ack) {
			bufferTransmision = bufferTransmision.slice(1,bufferTransmision.length);
			if(bufferTransmision.length == 0) {
				console.log("\nSe vacio el buffer de transmision...");
			}
		//}
	});
});


/* Este intervalo actualiza el tiempo del hecho global frente
 * a los cambios de hora.
 */
var actualizarTiempoHecho = setInterval(function () {
	var fecha = new Date();
	/* Se actualizan las fechas */
	hecho.tiempo.hora = fecha.getHours().toString() + ":" + fecha.getMinutes().toString();
	hecho.tiempo.diaMes = fecha.getDate();
	hecho.tiempo.mes = fecha.getMonth();
	hecho.tiempo.diaSemana = fecha.getDay();
	hechoAnterior = clone(hecho);
	//console.log(hecho);
	/* Se evalua el hecho nuevo */
	R.execute(hecho, function(result){
		for(i in hecho.nodos) {
			// Guardo los posibles comandos que devuelve el nodo
			var comando = hechoAnterior.nodos[i].actualizarEstado(hecho.nodos[i]);
			// Si hay comandos, los agrego al buffer de salida
			if(comando)
				bufferTransmision = Buffer.concat([bufferTransmision, comando]);
			//console.log(result);
		}
	});
}, 2000);

var sensarTemperatura = setInterval(function () {
	var comando = hecho.nodos.filter(function( obj ) { return obj.nombre == 'temperatura living';})[0].actualizarTemperatura();
	bufferTransmision = Buffer.concat([bufferTransmision, comando]);
},2000);
