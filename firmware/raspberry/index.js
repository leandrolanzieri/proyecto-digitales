/* Proyecto Domotica | Raspberry
 * index.js
 * Esta aplicacion de Node.js debe correr en la Raspberry Pi que sirve
 * como control central del sistema de domotica. Guardara las reglas para
 * establecer las escenas definidas por el usuario. Debera mantener una 
 * lista actualizada de los nodos disponibles y su estado.
 * 
 * Periodicamente se armara un 'fact' con el estado de todos los nodos,
 * la fecha y hora y sera evaluado por un rules-engine pasando por todas
 * las reglas establecidas. Asi se armara el comportamiento del sistema.
 */
/*******************************************************************************
 * Copyright (c) 2015  
 * Martin Enriquez (martin.hernan.enriquez@gmail.com),
 * Leandro Lanzieri Rodriguez (lanzierileandro@gmail.com).
 *
 * This file is part of Proyecto Domotica.
 *
 *     Proyecto Domotica is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Proyecto Domotica is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Proyecto Domotica.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
var WebSocket = require('ws');
var com = require('serialport');
var comunicacion = require('./comunicacion.json');

/* Los bytes para transmitir deben escribirse en este
 * buffer y seran enviados por uart en cada ocurrencia
 * del intervalo.
 */
var bufferTransmision = new Buffer([]);


/* Es el byte que se transmite en cada ocurrencia
 * del intervalo.
 */
var data = [];

var ws = new WebSocket('ws://probandosockets.herokuapp.com');
var serialPort = new com.SerialPort("/dev/ttyAMA0", {
	baudrate: 9600,
	stopBits: 1,
	parity: "none",
	parser: com.parsers.raw
  });


/* Intervalo para la transmision serie. Si hay
 * bytes en el buffer se escribe el de la
 * posicion 0.
 */
var intervalo = setInterval(function() {
	if(bufferTransmision.length != 0) {
		data[0] = bufferTransmision[0];
			console.log("\nTransmitiendo" + data);
			serialPort.write(data);
	}
}, 20);


serialPort.on('open', function() {
	console.log("Puerto Serie Abierto");
	
	  ws.on('open', function() {
		  ws.send(JSON.stringify({comando: "identificar", dato: "raspberry"}));
		  var ping = setInterval(function () {
			ws.send(JSON.stringify({comando: "ping", dato: "Rasp dice stay alive!"}));
		  }, 15000);
	  });
	  
	  ws.on('message', function(message) {
		  var mensaje = JSON.parse(message);
		  console.log("Comando recibido: " + mensaje.comando + " con el dato: " + mensaje.dato);
		  if(mensaje.comando == "rasptoggle") {
			comandos = new Buffer([218, 218, 218]); // Comandos a transmitir
			bufferTransmision = Buffer.concat([bufferTransmision, comandos]); // Se concatenan ak buffer de transmision
		  }
	  });
	  ws.on('close', function () {
		setInterval(function () {
			conectar();
		console.log("Intentando reconexion...");
		}, 5000);
	  });

	serialPort.on('data', function(mensaje) {
		console.log("\nSe recibe: " + mensaje);
		if(mensaje == comunicacion.ack) {
			bufferTransmision = bufferTransmision.slice(1,bufferTransmision.length);
			if(bufferTransmision.length == 0) {
				console.log("\nSe vacio el buffer de transmision...");
			}
		}
	});
}); 