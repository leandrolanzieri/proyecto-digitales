/*
 * Proyecto Domotica | Nodo Temperatura
 * main.c
 * El nodo temperatura debe poder leer los datos a la salida de
 * un LM35 haciendo uso del periferico ADC. Tambien debe poder
 * enviar estos datos a traves de un modulo NRF24L01+ con una
 * conexion SPI.
 */
/*******************************************************************************
 * Copyright (c) 2015
 * Martin Enriquez (martin.hernan.enriquez@gmail.com), 
 * Leandro Lanzieri Rodriguez (lanzierileandro@gmail.com).
 *
 * This file is part of Proyecto Domotica.
 *
 *     Proyecto Domotica is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Proyecto Domotica is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Proyecto Domotica.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/

#include "configuracion.h"
#include "comunicacion.h"
#include "hardware.h"
#include "nrf24l01.h"
#include "mcu.h"
uint8_t* data;
volatile unsigned int demora_tx=0;
unsigned char transmitido=0;

int main(void) {

	inicializarTodo();
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);  //Entre los modos se elige Power Down ya que apaga todos los clk,
										 //el micro se despertará asincronicamente
	Modo_RX(); // Cambia a recibir
	Set_frec(FREC_ON_OFF); // Cambia a frecuencia ON OFF
	ledBlink(); // Muestra que termino
	while(1) {

		if (demora_tx >= 20) { //Espera a que termine la transmision y vuelve a dormir.
			sleep_enable();
			reset();
			Modo_RX();
			CE = 1;
			demora_tx = 0;
			_delay_ms(20);
			sleep_cpu();
			sleep_disable();
		}

		if (!transmitido){  //Transmite una sola vez.
			data[0]=0xAA;
			Modo_TX();
			transmit_payload(data);
			transmitido =1;
		}

	}
}


ISR(INT0_vect)
{
	cli();
	CE = 0; // CE bajo
	data = WriteToNrf(R, R_RX_PAYLOAD, data, dataLen); //Lee el mensaje

	switch(data[0])  //Al buscar el mensaje en data[0] el mismo no contenia los datos correctos.
						//Buscando en el 1 se soluciona este inconveniente.
	{
		case RELE_ON:
			LED = 1;
			break;
		case RELE_OFF:
			LED = 0;
			break;
		case RELE_TOGGLE:
			LED ^= 1;
			break;
	}
	sei();
	CE = 1; // CE alto
	reset();
	return;
}

ISR (TIMER1_OVF_vect)
    {
    TCNT1H = 0xFC;
    TCNT1L = 0x17; //Tiempo para hacer que el timer cuente durante 1 mSeg
	demora_tx++;
	return;
    }
