/*
 * Proyecto Domotica | Nodo Temperatura
 * mcu.c
 * Contiene las funciones generales que se usan en el programa. En este caso de incializacion,
 * y encendido del led onboard.
 */
/*******************************************************************************
 * Copyright (c) 2015
 * Martin Enriquez (martin.hernan.enriquez@gmail.com), 
 * Leandro Lanzieri Rodriguez (lanzierileandro@gmail.com).
 *
 * This file is part of Proyecto Domotica.
 *
 *     Proyecto Domotica is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Proyecto Domotica is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Proyecto Domotica.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/

#include "configuracion.h"
#include "mcu.h"
#include "hardware.h"


void inicializarTodo() {
	inicializarGPIO();
	inicializarSPI();
	inicializarInterrupcionesExternas();
	inicializarNRF24L01();
	inicializarTimer();
	sei(); // Habilita interrupciones
}

void inicializarGPIO(void) {
	/* Led onboard */
	LED_DDR |= (1 << LED_PIN); // Led como salida
	LED = 1; // Se enciende con 0
}

void inicializarSPI(void) {

	/* Pines de comunicacion con NRF24L01 */
	SCK_DDR |= (1 << SCK_PIN); // SPI SCK como salida
	MOSI_DDR |= (1 << MOSI_PIN); // SPI MOSI como salida
	CE_DDR |= (1 << CE_PIN); // CE como salida
	CSN_DDR |= (1 << CSN_PIN); // CSN como salida

	/* Habilita comunicacion SPI y se configura al microcontrolador como master */
	SPCR |= (1 << SPE) | (1 << MSTR) | (1 << SPR0);

	CE = 1;
	CSN =1;
}

void inicializarTimer(void){
	TCCR1A = 0x00;
	TCCR1B = 0x02; //ARRANCA EL TIMER 1 CON PRESCALER DE OOOOOCHO
    TCNT1H = 0xFC;
    TCNT1L = 0x17; //Tiempo para hacer que el timer cuente durante 1 mSeg
}

void inicializarInterrupcionesExternas(void) {
	INT0_DDR &= ~(1 << INT0_PIN); // INT0 como entrada
	MCUCR |= (1 << ISC01); // INT0 detecta flanco descendente
	GICR |= (1 << INT0); // Habilito la interrupcion de INT0
}

/* Misc */
void ledBlink(void) {
	char i;
	for(i=0; i<6; i++) {
		LED ^= 1; //Toggle Led
		_delay_ms(155);
	}
}
