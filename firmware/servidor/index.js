var WebSocketServer = require("ws").Server;
var comunicacion = require("./comunicacion");
var http = require("http");
var express = require("express");
var app = express();
var port = process.env.PORT || 5000;

app.use(express.static(__dirname + "/public"));

var server = http.createServer(app)
server.listen(port)

console.log("\nServidor HTTP en el puerto %d.", port)

var wss = new WebSocketServer({server: server});
console.log("\nServidor Websocket creado.");

wss.broadcast = function broadcast(data) {
  wss.clients.forEach(function each(client) {
    client.send(data);
  });
};

wss.on("connection", function(ws) {
  console.log("\nConexion websocket nueva.")

  ws.on('message', function (data) {
    var mensaje = JSON.parse(data);
    if(mensaje.destino == 'servidor') {
    	/* La raspberry esta enviando actualizacion de estado o alerta,
    	 * se hace un broadcast a todos los usuarios con la actualizacion si
    	 * NO se esta enviando un ping.
    	 */
    	if(!mensaje.ping) {
    		console.log("\n Destino: " + mensaje.destino + " | Nombre del nodo: " + mensaje.nodo + " | Estado: " + mensaje.estado);
    		wss.broadcast(JSON.stringify({destino: "usuario", nodo: mensaje.nodo, estado: mensaje.estado}));
    	}
    }
    if(mensaje.destino == 'raspberry') {
    	/* Un usuario esta enviando algun comando
    	 * a la raspberry sobre algun nodo. Se hace
    	 * un broadcast pero dirigido a la raspberry con la actualizacion
    	 */
    	console.log("\n Destino: " + mensaje.destino + " | Nombre del nodo: " + mensaje.nodo + " | Comando: " + mensaje.comando);
    	wss.broadcast(JSON.stringify({destino: "raspberry", nodo: mensaje.nodo, comando: mensaje.comando}));
    }
  });

  ws.on("close", function() {
    console.log("\nSe cerro una conexion websocket.");
  })
})
