/*
 * Proyecto Domotica | Prueba ADC
 * adc.h
 * Contiene los prototipos de las funciones de adc.c
 */
/*******************************************************************************
 * Copyright (c) 2015
 * Martin Enriquez (martin.hernan.enriquez@gmail.com), 
 * Leandro Lanzieri Rodriguez (lanzierileandro@gmail.com).
 *
 * This file is part of Proyecto Domotica.
 *
 *     Proyecto Domotica is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Proyecto Domotica is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Proyecto Domotica.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/

#ifndef ADC_H_
#define ADC_H_

#define UN_VALOR	23

void inicializarADC(void);
unsigned char lectADCToGrados (unsigned char);
unsigned char gradosToLectADC (unsigned char);

unsigned char convertirADC0(void);

#endif /* ADC_H_ */
