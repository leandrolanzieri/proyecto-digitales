/*
 * Proyecto Domotica | Nodo Temperatura
 * main.c
 * El nodo temperatura debe poder leer los datos a la salida de
 * un LM35 haciendo uso del periferico ADC. Tambien debe poder
 * enviar estos datos a traves de un modulo NRF24L01+ con una
 * conexion SPI.
 */
/*******************************************************************************
 * Copyright (c) 2015
 * Martin Enriquez (martin.hernan.enriquez@gmail.com), 
 * Leandro Lanzieri Rodriguez (lanzierileandro@gmail.com).
 *
 * This file is part of Proyecto Domotica.
 *
 *     Proyecto Domotica is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Proyecto Domotica is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Proyecto Domotica.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/

#include "configuracion.h"
#include "comunicacion.h"
#include "hardware.h"
#include "nrf24l01.h"
#include "mcu.h"
#include "adc.h"
	unsigned int demora_tx=0;
	volatile unsigned char enviar=0;
	uint8_t* data;

int main(void) {
	unsigned char temperatura=200;
	inicializarTodo();
	Modo_RX(); 				// Cambia a transmitir
	Set_frec(FREC_ON_OFF);  // Cambia a frecuencia ON OFF
	ledBlink(); 			// Muestra que termino

	data[0] = ENVIAR_TEMP;
	data[1] = 200;
	while(1) {
		if (demora_tx >= 20) {
			reset();
			Modo_RX();
			CE = 1;
			demora_tx = 0;
			_delay_ms(20);
		}

		if (enviar==1){
			//temperatura = lectADCToGrados(convertirADC0());
			_delay_ms(100);

			data[0]=ENVIAR_TEMP;
			data[1]=temperatura;

			enviar =2;
			Modo_TX();
			transmit_payload(data);

		}
//		Modo_RX();
//		_delay_ms(20);
//		reset();
//		LED ^= 1;
	}
}


ISR(INT0_vect)
{

	cli();
	CE = 0; // CE bajo
	if (enviar == 0)
	{
			data = WriteToNrf(R, R_RX_PAYLOAD, data, dataLen); //Lee el mensaje
			switch(data[0])  //Al buscar el mensaje en data[0] el mismo no contenia los datos correctos. Buscando en el 1 se soluciona este inconveniente.
			{
				case MEDIR_TEMP:
					enviar =1;
					break;
				default:
					LED^=1;
			}
	}
	else enviar =0;
	sei();
	CE = 1; // CE alto
	reset();
	return;
}

ISR (TIMER1_OVF_vect)
    {
    TCNT1H = 0xFC;
    TCNT1L = 0x17; //Tiempo para hacer que el timer cuente durante 1 mSeg
	demora_tx++;
	return;
    }
