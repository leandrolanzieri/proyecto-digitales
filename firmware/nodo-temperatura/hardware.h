/*
 * Proyecto Domotica | Nodo Temperatura
 * hardware.h
 * Contiene las definiciones de pines, puertos y registros que son propios de
 * cada placa. Tambien una escructura de manejo de a bits.
 *
 */
/*******************************************************************************
 * Copyright (c) 2015
 * Martin Enriquez (martin.hernan.enriquez@gmail.com), 
 * Leandro Lanzieri Rodriguez (lanzierileandro@gmail.com).
 *
 * This file is part of Proyecto Domotica.
 *
 *     Proyecto Domotica is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Proyecto Domotica is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Proyecto Domotica.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/


#ifndef HARDWARE_H_
#define HARDWARE_H_

/**************************************Definiciones para Manejo de a bit**********************************/
typedef struct
	{
	unsigned char B0:1;
	unsigned char B1:1;
	unsigned char B2:1;
	unsigned char B3:1;
	unsigned char B4:1;
	unsigned char B5:1;
	unsigned char B6:1;
	unsigned char B7:1;
	}BIT;

#define REGBIT(reg,bt)	((volatile BIT*)&reg)->B##bt	//MACRO PARA MANEJAR REGISTROS DE A BIT.
/*********************************************************************************************************/

/* Led onboard */
#define LED_PORT	PORTD
#define LED_PIN		7
#define	LED_DDR		DDRD
#define LED			REGBIT(PORTD, 7)

/* Chip Enable del NRF24L01 */
#define CE_PORT		PORTB
#define CE_PIN		1
#define CE_DDR		DDRB
#define CE			REGBIT(PORTB, 1)

/* Chip Select Not del NRF24L01 */
#define CSN_PORT	PORTB
#define CSN_PIN		2
#define CSN_DDR		DDRB
#define CSN			REGBIT(PORTB, 2)

/* SPI MOSI */
#define MOSI_PORT	PORTB
#define MOSI_PIN	3
#define MOSI_DDR	DDRB
#define MOSI		REGBIT(PORTB, 3)

/* SPI MISO */
#define MISO_PORT	PORTB
#define MISO_PIN	4
#define MISO_DDR	DDRB
#define MISO		REGBIT(PORTB, 4)

/* SPI SCK */
#define SCK_PORT	PORTB
#define SCK_PIN		5
#define SCK_DDR		DDRB
#define SCK			REGBIT(PORTB, 5)

/* Interrupciones Externas Int0 */
#define INT0_PORT	PORTD
#define INT0_PIN	2
#define INT0_DDR	DDRD
#endif /* HARDWARE_H_ */
