/*
 * Proyecto Domotica | Nodo Central
 * nrf24l01.c
 * Contiene las funciones para comunicarse utilizando el modulo
 * NRF24L01+ a traves de SPI. Tambien funciones utilies para su
 * uso y configuracion.
 */
/*******************************************************************************
 * Copyright (c) 2015
 * Martin Enriquez (martin.hernan.enriquez@gmail.com), 
 * Leandro Lanzieri Rodriguez (lanzierileandro@gmail.com).
 *
 * This file is part of Proyecto Domotica.
 *
 *     Proyecto Domotica is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Proyecto Domotica is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Proyecto Domotica.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/

#include "configuracion.h"
#include "comunicacion.h"
#include "nrf24l01.h"
#include "hardware.h"

 char WriteByteSPI(unsigned char cData)
{
	SPDR = cData;

	while(!(SPSR & (1<<SPIF)));

	return SPDR;
}
/*
void PCINT12_interrupt_init(void)
{
	DDRD &= ~(1 << DDD2);		// INT0 (PD2)como entrada

	MCUCR |= (1 << ISC01); 		// INT0 en falling edge
	MCUCR &= ~(1 << ISC00);
	MCUCR &= ~(1 << ISC10);	// INT0 en falling edge

	GICR |= (1 << INT0); 						//Habilito la interrupci�n
}
*/

uint8_t GetReg(uint8_t reg)  //Funci�n para leer un registro reg
{

	_delay_us(10);
	CSN = 0;
	_delay_us(10);
	WriteByteSPI(R_REGISTER + reg);
	_delay_us(10);
	reg = WriteByteSPI(NOP);
	_delay_us(10);
	CSN = 1;
	return reg;
}


uint8_t *WriteToNrf(uint8_t ReadWrite, uint8_t reg, uint8_t *val, uint8_t antVal)	//Funci�n de lectura o escritura

//"W o R" (Escribir o leer), "reg" (el registro), "*val" (valor a escribir) & "antVal" (cantidad de enteros a escribir)

{
	cli();

	if (ReadWrite == W)
	{
		reg = W_REGISTER + reg;
	}

	static uint8_t ret[dataLen];

	_delay_us(10);
	CSN = 0;
	_delay_us(10);
	WriteByteSPI(reg);
	_delay_us(10);

	int i;
	for(i=0; i<antVal; i++)
	{
		if (ReadWrite == R && reg != W_TX_PAYLOAD)
		{
			ret[i]=WriteByteSPI(NOP);
			_delay_us(10);
		}
		else
		{
			WriteByteSPI(val[i]);
			_delay_us(10);
		}
	}
	CSN = 1;

	sei();

	return ret;
}

void inicializarNRF24L01(void)
{
	//_delay_ms(100);

	uint8_t val[5];

	val[0]=0x03;
	WriteToNrf(W, EN_AA, val, 1);	//Habilito auto ACK en P0


	val[0]=0x2F;
	WriteToNrf(W, SETUP_RETR, val, 1); //Configuro la retransmisi�n con un ARD=705useg y ARC=15

	val[0]=0x03;
	WriteToNrf(W, EN_RXADDR, val, 1); //Habilito el P0

	val[0]=0x03;
	WriteToNrf(W, SETUP_AW, val, 1); //Seteo 5 bytes de longitud de direcci�n

	val[0]=FREC_ON_OFF;
	WriteToNrf(W, RF_CH, val, 1); //Seteo el canal de frecuencia 2,502 GHz

	val[0]=0x26;
	WriteToNrf(W, RF_SETUP, val, 1); //Seteo vel de transmisi�n en 250Kb/s y potencia en 0dBm

	int i;
	for(i=0; i<5; i++)
	{
		val[i]=0x12;	//N�mero de direcci�n
	}
	WriteToNrf(W, RX_ADDR_P0, val, 5); //Cargo la direcci�n del RX
	
	for(i=0; i<5; i++)
	{
		val[i]=0x13;	//N�mero de direcci�n
	}
	WriteToNrf(W, RX_ADDR_P1, val, 5); //Cargo la direcci�n del RX

	for(i=0; i<5; i++)
	{
		val[i]=0x12;	//N�mero de direcci�n
	}
	WriteToNrf(W, TX_ADDR, val, 5);  //Cargo la direcci�n del TX

	// Configuraci�n de capacidad de la carga �til (payload) - Est�tica de 1 - 32 bytes
	val[0]=dataLen;
	WriteToNrf(W, RX_PW_P0, val, 1);
	
	val[0]=dataLen;
	WriteToNrf(W, RX_PW_P1, val, 1);


	val[0]=0x1F;  //Configuraci�n de modo, TX=7E - RX=1F
	WriteToNrf(W, CONFIG, val, 1);

	//_delay_ms(100);

}

void reset(void)
{
	_delay_us(10);
	CSN = 0;
	_delay_us(10);
	WriteByteSPI(W_REGISTER + STATUS);
	_delay_us(10);
	WriteByteSPI(0b01110000);
	_delay_us(10);
	CSN = 1;
}


void receive_payload(void)
{
	sei();

	CE = 1;
	_delay_ms(1000);
	CE = 0;

	cli();
}

void transmit_payload(uint8_t * W_buff)
{
	WriteToNrf(R, FLUSH_TX, W_buff, 0);

	WriteToNrf(R, W_TX_PAYLOAD, W_buff, dataLen);

	sei();

	_delay_ms(10);
	CE = 1;
	_delay_ms(1);
	CE = 0;
	_delay_ms(10);

}

void Modo_TX(void)
{
	uint8_t val[5];
	val[0]=0x7E;  //Configuraci�n de modo, TX=7E - RX=1F
	WriteToNrf(W, CONFIG, val, 1);
	CE=0;
}

void Modo_RX(void)
{
	uint8_t val[5];
	val[0]=0x1F;  //Configuraci�n de modo, TX=7E - RX=1F
	WriteToNrf(W, CONFIG, val, 1);
} 

void Set_frec(uint8_t frecuencia)
{
	uint8_t val[5];	
	val[0]= frecuencia;
	WriteToNrf(W, RF_CH, val, 1); 
}

void Cambio_Pipe(unsigned char direccion)
{
	uint8_t val[5];
	int i;
	for(i=0; i<5; i++)
	{
		val[i]=direccion;	//N�mero de direcci�n
	}
	WriteToNrf(W, TX_ADDR, val, 5);  //Cargo la direcci�n del TX
} 
