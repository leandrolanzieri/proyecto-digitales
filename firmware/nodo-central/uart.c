/*******************************************************************************
 * Copyright (c) 2015
 * Martin Enriquez (martin.hernan.enriquez@gmail.com), 
 * Leandro Lanzieri Rodriguez (lanzierileandro@gmail.com).
 *
 * This file is part of Proyecto Domotica.
 *
 *     Proyecto Domotica is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Proyecto Domotica is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Proyecto Domotica.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/*
 * Proyecto Domotica | Nodo Central
 * uart.c
 * Contiene las funciones referidas al uso, inicializacion y
 * configuracion de la UART.
 */
#include "configuracion.h"
#include "uart.h"
#include "hardware.h"
#include "comunicacion.h"
#include "nrf24l01.h"

/* inicializarUART
 * Configura los registros necesarios para
 * inicializar el periferico de comunicacion
 * serie UART.
 */
unsigned char datos[5];

void inicializarUART() {

	/* Configura el Baud Rate - ver configuracion.h */

/*	UBRRH = (unsigned char) (UBRRREG >> 8);
	UBRRL = (unsigned char) UBRRREG;*/
	/*Para ATMEGA328:
	 */	UBRR0H = (unsigned char) (UBRRREG >> 8);
		UBRR0L = (unsigned char) UBRRREG;


	/* Habilita recepcion y transmision, e interrupcion de recepcion */
//	UCSRB = (1 << RXEN) | (1 << TXEN) | (1 << RXCIE);
	UCSR0B = (1 << RXEN0) | (1 << TXEN0) | (1 << RXCIE0);

	/* Comunicacion de 8 bits, 1 bits stop */
//	UCSRC = (1 << URSEL) | (3 << UCSZ0);
	UCSR0C = (3 << UCSZ00);
}


/* transmitirByteUART
 * Envia un byte por UART. Recibe el dato a enviar
 */
void transmitirByteUART(unsigned char dato) {
	/* Espero a que este libre el buffer */
	//while(! (UCSRA & (1<<UDRE)));
	while(! (UCSR0A & (1<<UDRE0)));

	/* Coloco el dato en el buffer */
	//UDR = dato;
	UDR0 = dato;
}


/* recibirByteUART
 * Recibe un byte por UART. Devuelve el dato
 * recibido.
 */
unsigned char recibirByteUART(void) {
	/* Espero a que este recibido el dato */
	//while(! (UCSRA & (1<<RXC)));
	while(! (UCSR0A & (1<<RXC0)));

	/* Devuelvo el dato */
	//return UDR;
	return UDR0;
}


/* disponibleUART
 * Devuelve si hay algun dato disponible para
 * lectura en la UART.
 */
unsigned char disponibleUART(void) {
	//return (! (UCSRA & (1<<RXC)));
	return (! (UCSR0A & (1<<RXC0)));
}


/* transmitirUART
 * Transmite un vector de bytes por la UART.
 */
void transmitirUART(unsigned char * datos) {
	int i;
	for(i=0; i<(sizeof(datos)/sizeof(datos[0])); i++) {
		transmitirByteUART(datos[i]);
	}
}


unsigned char comandoValido(unsigned char comando) {
	switch(comando) {
		case RELE_ON:
		case RELE_OFF:
		case RELE_TOGGLE:
		case LUCES_ON:
		case LUCES_OFF:
		case LUCES_TOGGLE:
		case LUCES_DUTY:
		case LUCES_AUMENTAR:
		case LUCES_DISMINUIR:
		case PING:
		case ACK:
		case MEDIR_TEMP:
		case ENVIAR_TEMP:
			return 1;
			break;
		default:
			return 0;
	}
}

/* Rutina de interrupcion por recepcion
 * de UART.
 */
ISR(USART_RX_vect) {
		unsigned char datos[5];
		unsigned char dato = recibirByteUART();
		if(comandoValido(dato)) {
			reset();
			Modo_TX();
			datos[0] = dato;
			datos[1] = PING;
			datos[2] = PING;
			datos[3] = PING;
			datos[4] = PING;
			transmitirByteUART(ACK);
			transmit_payload(datos); // Envia los datos
			/* NOTA: Este delay y reset hacen la comunicacion estable, si no estan
			 * se produce una falla permanente o perdida de datos. Se evaluara colocarlos
			 * dentro de la funcion de envio de paquetes.
			 */
			_delay_ms(20);
			reset();
			Modo_RX();
		}
}
