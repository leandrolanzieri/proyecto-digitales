/*******************************************************************************
 * Copyright (c) 2015
 * Martin Enriquez (martin.hernan.enriquez@gmail.com), 
 * Leandro Lanzieri Rodriguez (lanzierileandro@gmail.com).
 *
 * This file is part of Proyecto Domotica.
 *
 *     Proyecto Domotica is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Proyecto Domotica is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Proyecto Domotica.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/*
 * Proyecto Domotica | Nodo Central
 * uart.h
 * Contiene los prototipos de las funciones de uart.c
 */

#ifndef UART_H_
#define UART_H_

/* Inicializacion */
void inicializarUART(void);

/* Comunicacion */
void transmitirByteUART(unsigned char);
unsigned char recibirByteUART(void);
unsigned char disponibleUART(void);
void transmitirUART(unsigned char *);
unsigned char comandoValido(unsigned char);
#endif /* UART_H_ */
