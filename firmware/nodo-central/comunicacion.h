/*
 * Proyecto Domotica | Nodo Central
 * comunicacion.h
 * Contiene los comandos que son interpretables por todos los nodos
 * de la red. Tambien las frecuencias predefinidas para cada tipo
 * de nodo.
 */
/*******************************************************************************
 * Copyright (c) 2015
 * Martin Enriquez (martin.hernan.enriquez@gmail.com), 
 * Leandro Lanzieri Rodriguez (lanzierileandro@gmail.com).
 *
 * This file is part of Proyecto Domotica.
 *
 *     Proyecto Domotica is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Proyecto Domotica is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Proyecto Domotica.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/


#ifndef COMUNICACION_H_
#define COMUNICACION_H_

/* ======== COMANDOS ========= */
#define RELE_ON		0x0F
#define	RELE_OFF	0x01
#define	RELE_TOGGLE	0xDA
#define ENTRO_PANTALLA	0x07

#define	LUCES_ON	0x1F
#define	LUCES_OFF	0x11
#define	LUCES_TOGGLE 0xDD
#define LUCES_DUTY	0x1D
#define LUCES_AUMENTAR 0x1B
#define	LUCES_DISMINUIR 0x1C

#define MEDIR_TEMP 0xDB
#define ENVIAR_TEMP 'B'

#define PING 0x19
#define ACK 0x41
/* ======= FRECUENCIAS ======== */
/* El MSB debe ser 0*/
#define FREC_LUCES	0x66 // 0110 0110
#define	FREC_ON_OFF	0x78 // 0111 1000

#endif /* COMUNICACION_H_ */
