/*
 * Proyecto Domotica | Nodo Central
 * main.c
 * El nodo central debe poder comunicarse a traves de la UART para poder
 * enviar y recibir comandos e informacion desde el control central del
 * sistema de domotica.
 *
 * Tambien debe establecer una conexion de tipo estrella con los demas
 * nodos para enviar y recibir comandos e informacion. La conexion con
 * los nodos es inalambrica y se realiza utilizando el modulo NRF24L01+,
 * a traves de SPI.
 */
/*******************************************************************************
 * Copyright (c) 2015
 * Martin Enriquez (martin.hernan.enriquez@gmail.com), 
 * Leandro Lanzieri Rodriguez (lanzierileandro@gmail.com).
 *
 * This file is part of Proyecto Domotica.
 *
 *     Proyecto Domotica is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Proyecto Domotica is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Proyecto Domotica.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/

#include "configuracion.h"
#include "comunicacion.h"
#include "hardware.h"
#include "uart.h"
#include "mcu.h"
#include "nrf24l01.h"

uint8_t* data;
int main(void) {

	inicializarTodo();
	Modo_RX(); // Cambia a transmitir
	Set_frec(FREC_ON_OFF);
	ledBlink();
	while(1) {

	}
}

ISR(INT0_vect)
{

	cli();
	CE = 0; // CE bajo
	data = WriteToNrf(R, R_RX_PAYLOAD, data, dataLen); //Lee el mensaje

	switch(data[0])
	{
		case ENVIAR_TEMP:
			// Llega temperatura del nodo
			transmitirByteUART(data[0]);
			transmitirByteUART(data[1]);
			ledBlink();
			break;

	}

	sei();
	CE = 1; // CE alto
	reset();
}
