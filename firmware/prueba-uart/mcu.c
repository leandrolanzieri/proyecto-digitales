/*
 * Proyecto Domotica | Prueba UART
 * mcu.c
 * Contiene las funciones generales que se usan en el programa. En este caso de incializacion,
 * uso de UART y encendido del led onboard.
 */
/*******************************************************************************
 * Copyright (c) 2015
 * Martin Enriquez (martin.hernan.enriquez@gmail.com), 
 * Leandro Lanzieri Rodriguez (lanzierileandro@gmail.com).
 *
 * This file is part of Proyecto Domotica.
 *
 *     Proyecto Domotica is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Proyecto Domotica is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Proyecto Domotica.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/

#include "mcu.h"
#include "hardware.h"
#include "configuracion.h"

void inicializarTodo() {
	inicializarUART();
	inicializarGPIO();
	sei(); // Habilita interrupciones
}

void inicializarGPIO(void) {
	LED_DDR |= (1 << LED_PIN); // Led como salida
	LED = 1; // Se enciende con 0
}

void inicializarUART() {

	/* Configura el Baud Rate - ver configuracion.h */
	UBRRH = (unsigned char) (UBRRREG >> 8);
	UBRRL = (unsigned char) UBRRREG;

	/* Habilita recepcion y transmision, e interrupcion de recepcion */
	UCSRB = (1 << RXEN) | (1 << TXEN) | (1 << RXCIE);

	/* Comunicacion de 8 bits, 1 bits stop */
	UCSRC = (1 << URSEL) | (3 << UCSZ0);
}

/* Misc */
void ledBlink(void) {
	char i;
	for(i=0; i<6; i++) {
		LED ^= 1; //Toggle Led
		_delay_ms(155);
	}
}

/* Funciones UART */
void transmitirByteUART(unsigned char dato) {
	/* Espero a que este libre el buffer */
	while(! (UCSRA & (1<<UDRE)));

	/* Coloco el dato en el buffer */
	UDR = dato;
}

unsigned char recibirByteUART(void) {
	/* Espero a que este recibido el dato */
	while(! (UCSRA & (1<<RXC)));

	/* Devuelvo el dato */
	return UDR;
}

unsigned char disponibleUART(void) {
	return (! (UCSRA & (1<<RXC)));
}

void transmitirUART(unsigned char * datos) {
	int i;
	for(i=0; i<strlen(datos); i++) {
		transmitirByteUART(datos[i]);
	}
}
