/* Proyecto Domotica | Prueba UART
 * configuracion.h
 * Importa las librerias propias de AVR. Define constantes de configuracion del funcionamiento
 * general del programa, como la frecuencia del cristal utilizado y el baud rate para la
 * comunicacion serial.
 */
/*******************************************************************************
 * Copyright (c) 2015
 * Martin Enriquez (martin.hernan.enriquez@gmail.com), 
 * Leandro Lanzieri Rodriguez (lanzierileandro@gmail.com).
 *
 * This file is part of Proyecto Domotica.
 *
 *     Proyecto Domotica is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Proyecto Domotica is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Proyecto Domotica.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/


#ifndef CONFIGURACION_H_
#define CONFIGURACION_H_

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdio.h>
#include <string.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#define FOSC 8000000 // 8MHz Frecuencia de oscilacion
#define BAUD 9600 // Baud Rate
#define UBRRREG ((FOSC/16)/BAUD)-1 // El reultado debe ser el numero del registro UBRR para tener BAUD


#endif /* CONFIGURACION_H_ */
