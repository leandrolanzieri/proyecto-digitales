/*
 * Proyecto Domotica | Prueba UART
 * hardware.h
 * Contiene las definiciones de pines, puertos y registros que son propios de
 * cada placa. Tambien una escructura de manejo de a bits.
 *
 */
/*******************************************************************************
 * Copyright (c) 2015
 * Martin Enriquez (martin.hernan.enriquez@gmail.com), 
 * Leandro Lanzieri Rodriguez (lanzierileandro@gmail.com).
 *
 * This file is part of Proyecto Domotica.
 *
 *     Proyecto Domotica is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Proyecto Domotica is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Proyecto Domotica.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/


#ifndef HARDWARE_H_
#define HARDWARE_H_

/**************************************Definiciones para Manejo de a bit**********************************/
typedef struct
	{
	unsigned char B0:1;
	unsigned char B1:1;
	unsigned char B2:1;
	unsigned char B3:1;
	unsigned char B4:1;
	unsigned char B5:1;
	unsigned char B6:1;
	unsigned char B7:1;
	}BIT;

#define REGBIT(reg,bt)	((volatile BIT*)&reg)->B##bt	//MACRO PARA MANEJAR REGISTROS DE A BIT.
/*********************************************************************************************************/

#define LED_PORT	PORTD
#define LED_PIN		7
#define	LED_DDR		DDRD
#define LED			REGBIT(PORTD, 7)


#endif /* HARDWARE_H_ */
