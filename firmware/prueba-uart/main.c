/* Proyecto Domotica | Prueba UART
 * main.c
 * Archivo principal del proyecto. Inicializa el microcontrolador. Sirve como ejemplo de uso
 * y para realizar varias pruebas sobre el modulo UART.
 */
/*******************************************************************************
 * Copyright (c) 2015
 * Martin Enriquez (martin.hernan.enriquez@gmail.com), 
 * Leandro Lanzieri Rodriguez (lanzierileandro@gmail.com).
 *
 * This file is part of Proyecto Domotica.
 *
 *     Proyecto Domotica is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Proyecto Domotica is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Proyecto Domotica.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/


#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdio.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "hardware.h"
#include "configuracion.h"
#include "mcu.h"

unsigned char data,flag=0;
int main (void) {
	inicializarTodo();
	ledBlink();

	while(1){

	}
	return 0;
}

ISR(USART_RXC_vect) {
	data = recibirByteUART();
	if(data == 0xDA)
	{
		LED ^= 1;
		transmitirByteUART(data);
	}
}
