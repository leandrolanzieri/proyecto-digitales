/*******************************************************************************
 * Copyright (c) 2015
 * Martin Enriquez (martin.hernan.enriquez@gmail.com), 
 * Leandro Lanzieri Rodriguez (lanzierileandro@gmail.com).
 *
 * This file is part of Proyecto Domotica.
 *
 *     Proyecto Domotica is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Proyecto Domotica is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Proyecto Domotica.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
/*
 * Proyecto Domotica | Prueba ADC
 * adc.c
 * Funciones de configuracion y uso del ADC.
 */
#include "configuracion.h"
#include "adc.h"
#define	VCC	3.3

void inicializarADC(void) {
	ADMUX |= (3 << REFS0); // Tension de referencia= Vcc (3,3) y ajuste a la izquierda
	// Se usa ADC0
	ADCSRA |= (1 << ADEN); // Habilita el ADC
}

void disableADC(void){
	ADCSRA &= ~(1 << ADEN); //Deshabilita el ADC
}

unsigned char convertirADC0(void) {
	ADCSRA |= (1 << ADSC); // Inicia la conversion
	while(!ADIF){} // Espera a que termine de convertir
	return ADCH; // Devuelve los 8 bits mas significativos
}


unsigned char gradosToLectADC(unsigned char grados)
{
	int lectura;
	lectura = grados * 256 / (VCC*100);
	if (lectura>255) lectura = 255;
	if (lectura <0) lectura =0;
	return (unsigned char)lectura;
}

unsigned char lectADCToGrados(unsigned char lectura)
{
	int grados;
	grados = lectura * VCC * 100 / 256;
	if (grados>150) grados= 150;
	if (grados <0) grados=0;
	return (unsigned char)grados;
}
