/*
 * Proyecto Domotica | Prueba ADC
 * mcu.c
 * Contiene las funciones generales que se usan en el programa. En este caso de incializacion,
 * uso de ADC y encendido del led onboard.
 */
/*******************************************************************************
 * Copyright (c) 2015
 * Martin Enriquez (martin.hernan.enriquez@gmail.com), 
 * Leandro Lanzieri Rodriguez (lanzierileandro@gmail.com).
 *
 * This file is part of Proyecto Domotica.
 *
 *     Proyecto Domotica is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Proyecto Domotica is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Proyecto Domotica.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/

#include "mcu.h"
#include "hardware.h"
#include "configuracion.h"
#include "adc.h"

void inicializarTodo() {
	inicializarGPIO();
	inicializarADC();
	sei(); // Habilita interrupciones
}

void inicializarGPIO(void) {
	LED_DDR |= (1 << LED_PIN); // Led como salida
	LED = 1; // Se enciende con 0
}


/* Misc */
void ledBlink(void) {
	char i;
	for(i=0; i<6; i++) {
		LED ^= 1; //Toggle Led
		_delay_ms(155);
	}
}
