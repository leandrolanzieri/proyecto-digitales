/*
 * main.c
 *
 *  Created on: 25/1/2015
 *      Author: Enr�quez Mart�n | Leandro Lanzieri
 */

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdio.h>
#include <util/delay.h>

#include <avr/interrupt.h>
#include "nrf24l01.h"
#include "mcu.h"
#include "hardware.h"
#include "comunicacion.h"


uint8_t *data;
volatile boton_t boton;

int main(void)
{
	init_megazord();
	LED = 1;
	int i;
	// El mirco est� vivo... EST� VIVOOO! (?)
	for(i = 0; i < 6; i++)
	{
		LED ^= 1; // Toggle LED
		_delay_ms(200);
	}
	Set_frec(FREC_LUCES);

	while(1)
	{

		if (demora_tim1 >= 20)
		{
			reset();
			Modo_RX();
			CE = 1;
			demora_tim1 = 0;
		}
		// Como prueba, envio un toggle al rele cada medio segundo
		// Para esto elimine la lectura de los botones.
		_delay_ms(500);
		data[0] = LUCES_TOGGLE;
		data[1] = LUCES_TOGGLE;
		data[2] = LUCES_TOGGLE;
		data[3] = LUCES_TOGGLE;
		data[4] = LUCES_TOGGLE;
		Modo_TX();
		transmit_payload(data);
		reset();
		demora_tx = 0;

/*
		leer_botones();
		switch(boton)
		{
			case LUZ:
				Set_frec(FREC_LUCES);
				_delay_ms(20);
				data[0] = LUCES_TOGGLE;
				Modo_TX();
				transmit_payload(data);
				reset();
				demora_tx = 0;
				break;

			case ENCHUFE:
				Set_frec(FREC_ON_OFF);
				_delay_ms(20);
				data[0] = RELE_TOGGLE;
				Modo_TX();
				transmit_payload(data);
				reset();
				demora_tx = 0;
				break;
			case INCREMENTAR:
				LED ^= 1;
				Set_frec(FREC_LUCES);
				_delay_ms(20);
				data[0] = LUCES_AUMENTAR;
				Modo_TX();
				transmit_payload(data);
				reset();
				demora_tx = 0;
				break;
			case DECREMENTAR:
				Set_frec(FREC_LUCES);
				_delay_ms(20);
				data[0] = LUCES_DISMINUIR;
				Modo_TX();
				transmit_payload(data);
				reset();
				demora_tx = 0;
				break;
			case NINGUNO:
			default:
				break;
		}*/

	}

	return 0;
}


ISR(INT0_vect)
{
	cli();
	CE = 0; // CE bajo

	data = WriteToNrf(R, R_RX_PAYLOAD, data, dataLen); //Leo el mensaje
	if(data[1] == LUCES_ON) LED = 1;
	if(data[1] == LUCES_OFF) LED = 0;
	data[1] = 0;
	sei();
	CE = 1; // CE alto
}

