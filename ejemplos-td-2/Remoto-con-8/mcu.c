/*
 * mcu.c
 *
 *  Created on: 28/1/2015
 *      Author: Enr�quez Mart�n | Leandro Lanzieri
 */

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "nrf24l01.h"
#include "mcu.h"
#include "hardware.h"


volatile unsigned int demora_tim1=0;
volatile unsigned int demora_tx=0;
volatile unsigned int tiempo_tx=0;
extern volatile boton_t boton;

void init_megazord(void)
{
	TCCR1A = 0x00;
	TCCR1B = 0x02; //ARRANCA EL TIMER 1 CON PRESCALER DE OOOOOCHO
    TCNT1H = 0xFC;
    TCNT1L = 0x17; //Tiempo para hacer que el timer cuente durante 1 mSeg
	init_SPI();
	nrf24L01_init();
	init_INT();
	LED_DDR |= (1 << LED_PIN); // LED como salida
	BTN_ENCH_DDR &= ~(1<<BTN_ENCH_PIN);
	BTN_INC_DDR &= ~(1<<BTN_INC_PIN);
	BTN_DEC_DDR &= ~(1<<BTN_DEC_PIN);
	BTN_LUZ_DDR &= ~(1<<BTN_LUZ_PIN);

	TIMSK = 0x04; //Habilitar interrupcion por ovf del timer1
	sei();
}


void init_SPI(void)
{
	// Pongo SCK, MOSI, CSN y CE como salidas
	SCK_DDR |= (1 << SCK_PIN);

	MOSI_DDR |= (1 << MOSI_PIN);

	CE_DDR |= (1 << CE_PIN);

	CSN_DDR |= (1 << CSN_PIN);

	// Habilito SPI y pongo el micro como master
	SPCR |= (1 << SPE) | (1 << MSTR) | (1 << SPR0);

	CSN = 1;
	CE = 1;
}


void init_INT(void)
{
	DDRD &= ~(1 << DDD2);		// INT0 (PD2) e INT1 (PD3) como entradas
	DDRD &= ~(1 << DDD3);

	MCUCR |= (1 << ISC01) | (1 << ISC11); 		// INT0 e INT1 en falling edge
	MCUCR &= ~(1 << ISC00);
	MCUCR &= ~(1 << ISC10);	// INT0 e INT1 en falling edge

	GICR |= (1 << INT0); 						//Habilito la interrupci�n
}


ISR (TIMER1_OVF_vect)
    {
    TCNT1H = 0xFC;
    TCNT1L = 0x17; //Tiempo para hacer que el timer cuente durante 1 mSeg
    demora_tim1++;
	demora_tx++;
	tiempo_tx++;
    }

/*
 * Delay_ms
 * Brief: Funcion de retardo
 */

void delay_ms(unsigned int tiempo)
    {
    demora_tim1 = 0;
    while(demora_tim1 < tiempo );

    }

void leer_botones (void)
{
	boton = NINGUNO;
	if(BTN_DEC)
	{
		_delay_ms(75);
		if(BTN_DEC)
			boton = DECREMENTAR;
	}
	if(BTN_INC)
	{
		_delay_ms(75);
		if(BTN_INC)
			boton = INCREMENTAR;
	}
	if(BTN_LUZ)
	{
		_delay_ms(75);
		if(BTN_LUZ)
			boton = LUZ;
	}
	if(BTN_ENCH)
	{
		_delay_ms(75);
		if(BTN_ENCH)
			boton = ENCHUFE;
	}
}


