/*
 * mcu.h
 *
 *  Created on: 28/1/2015
 *      Author: Enr�quez Mart�n | Leandro Lanzieri
 */

#ifndef MCU_H_
#define MCU_H_

typedef enum
{
	ENCHUFE,
	LUZ,
	INCREMENTAR,
	DECREMENTAR,
	NINGUNO
}boton_t;

void leer_botones(void);
void init_SPI (void);
void init_INT(void);
void init_megazord(void);
ISR(INT0_vect);
ISR(INT1_vect);
extern volatile unsigned int demora_tim1;
extern volatile unsigned int demora_tx;
extern volatile unsigned int tiempo_tx;
#endif /* MCU_H_ */
