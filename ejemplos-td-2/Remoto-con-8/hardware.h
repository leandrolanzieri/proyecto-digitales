/*
 * hardware.h
 *
 *  Created on: 28/1/2015
 *      Author: Enr�quez Mart�n | Leandro Lanzieri
 */

#ifndef HARDWARE_H_
#define HARDWARE_H_

/**************************************Definiciones para Manejo de a bit**********************************/
typedef struct
	{
	unsigned char B0:1;
	unsigned char B1:1;
	unsigned char B2:1;
	unsigned char B3:1;
	unsigned char B4:1;
	unsigned char B5:1;
	unsigned char B6:1;
	unsigned char B7:1;
	}BIT;

#define REGBIT(reg,bt)	((volatile BIT*)&reg)->B##bt	//MACRO PARA MANEJAR REGISTROS DE A BIT.
/*********************************************************************************************************/

#define LED_PORT	PORTB
#define LED_PIN		0
#define	LED_DDR		DDRB
#define LED			REGBIT(PORTB, 0)

#define BTN_ENCH_PORT	PINB
#define BTN_ENCH_PIN	7
#define	BTN_ENCH_DDR	DDRB
#define BTN_ENCH		REGBIT(PINB, 7)

#define BTN_LUZ_PORT	PIND
#define BTN_LUZ_PIN		5
#define	BTN_LUZ_DDR		DDRD
#define BTN_LUZ			REGBIT(PIND, 5)

#define BTN_INC_PORT	PORTD
#define BTN_INC_PIN		6
#define	BTN_INC_DDR		DDRD
#define BTN_INC			REGBIT(PIND, 6)

#define BTN_DEC_PORT	PORTD
#define BTN_DEC_PIN		7
#define	BTN_DEC_DDR		DDRD
#define BTN_DEC			REGBIT(PIND, 7)

#define CSN_PORT	PORTB
#define CSN_PIN		2
#define	CSN_DDR		DDRB
#define CSN			REGBIT(PORTB, 2)

#define CE_PORT		PORTB
#define CE_PIN		1
#define	CE_DDR		DDRB
#define CE			REGBIT(PORTB, 1)

#define MOSI_PORT		PORTB
#define MOSI_PIN		3
#define	MOSI_DDR		DDRB

#define MISO_PORT		PORTB
#define MISO_PIN		4
#define	MISO_DDR		DDRB

#define SCK_PORT		PORTB
#define SCK_PIN			5
#define	SCK_DDR			DDRB

#endif /* HARDWARE_H_ */
