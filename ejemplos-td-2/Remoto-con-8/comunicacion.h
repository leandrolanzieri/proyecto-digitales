/*
 * comunicacion.h
 *
 *  Created on: 11/2/2015
 *      Author: Mart�n
 */

#ifndef COMUNICACION_H_
#define COMUNICACION_H_

/* ======== COMANDOS ========= */
#define RELE_ON		0x0F
#define	RELE_OFF	0x01
#define	RELE_TOGGLE	0xDA
#define ENTRO_PANTALLA	0x07

#define	LUCES_ON	0x1F
#define	LUCES_OFF	0x11
#define	LUCES_TOGGLE 0x1A
#define LUCES_DUTY	0x1D
#define LUCES_AUMENTAR 0x1B
#define	LUCES_DISMINUIR 0x1C

/* ======= FRECUENCIAS ======== */
#define FREC_LUCES	0x66
#define	FREC_ON_OFF	0x78

#endif /* COMUNICACION_H_ */
