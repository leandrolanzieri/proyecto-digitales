/*
 * Proyecto de Domotica - Tecnicas Digitales III
 * Modulo On Off
 * Este programa utiliza los comandos que recibe a traves
 * del modulo NRF24L01+ para modificar el estado de
 * una salida de manera inalambrica.
 *
 * NOTA: Modificar configuraciones de frecuencia, rol y
 * pines en hardware.h
 */
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "nrf24l01.h"
#include "mcu.h"
#include "hardware.h"
#include "comunicacion.h"

volatile unsigned char rele = 0, enviar_estado = 0;
uint8_t *data;

int main(void)
{
	init_megazord();
	int i;

	for (i = 0; i < 11; i++)
	{
		LED ^= 1;
		_delay_ms(200);
	}

	while(1)
	{
//		RELE = rele? 1 : 0; // Debo encender el rel�?



/* 		Esto se quitó y sigue funcionando. Lunes 30/11/2015.
 * 		Posiblemente no se incluya en proximas versiones.
 * 		if (demora_tx >= 20)
		{
			reset();
			Modo_RX();
			CE = 1;
			demora_tx = 0;
			_delay_ms(20);
		}
*/

	}
	return 0;
}


ISR(INT0_vect)
{
	cli();
	CE = 0; // CE bajo
	LED ^= 1;
	data = WriteToNrf(R, R_RX_PAYLOAD, data, dataLen); //Leo el mensaje

	switch(data[1])  //Al buscar el mensaje en data[0] el mismo no contenia los datos correctos. Buscando en el 1 se soluciona este inconveniente.
	{
		case RELE_ON:
			rele = 1;
			break;
		case RELE_OFF:
			rele = 0;
			break;
		case RELE_TOGGLE:
			RELE ^= 1;
			break;
	}

	data[0] = 0;
	sei();
	CE = 1; // CE alto
	reset();
}


ISR (TIMER1_OVF_vect)
{
    TCNT1H = 0xFC;
    TCNT1L = 0x17; //Tiempo para hacer que el timer cuente durante 1 mSeg
    demora_tim1++;
	demora_tx++;
	tiempo_tx++;
}
