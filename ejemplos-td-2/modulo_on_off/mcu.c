/*
 * mcu.c
 *
 *  Created on: 5/2/2015
 *      Author: Mart�n
 */
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "nrf24l01.h"
#include "mcu.h"
#include "hardware.h"

volatile unsigned int demora_tim1=0;
volatile unsigned int demora_tx=0;
volatile unsigned int tiempo_tx=0;

void init_megazord(void)
{
	LED_DDR |= (1 << LED_PIN); //LED SALIDA

	RELE_DDR |= (1 << RELE_PIN); //RELE SALIDA

	init_TMR();
    init_SPI();
    nrf24L01_init();
    _delay_ms(300);
    init_INT();
    sei();
}


void init_TMR(void)
{
	//Timer1 para SystemTick
	TCCR1A = 0x00;
    TCNT1H = 0xFC;
    TCNT1L = 0x17; //Tiempo para hacer que el timer cuente durante 1 mSeg
    TIMSK = 0x04; //Habilitar interrupcion por ovf del timer1
	TCCR1B = 0x02; //ARRANCA EL TIMER 1 CON PRESCALER DE OOOOOCHO
}


void init_SPI(void)
{
	// Pongo SCK, MOSI, CSN y CE como salidas
	SCK_DDR |= (1 << SCK_PIN);

	MOSI_DDR |= (1 << MOSI_PIN);

	CE_DDR |= (1 << CE_PIN);

	CSN_DDR |= (1 << CSN_PIN);

	// Habilito SPI y pongo el micro como master
	SPCR |= (1 << SPE) | (1 << MSTR) | (1 << SPR0);

	CSN = 1;
	CE = 1;
}


void init_INT(void)
{
	DDRD &= ~(1 << DDD2);		// INT0 (PD2) como entradas

	MCUCR |= (1 << ISC01); 		// INT0 e INT1 en falling edge
	MCUCR &= ~(1 << ISC00);

	GICR |= (1 << INT0); 		//Habilito la interrupci�n
}

/*
 * Delay_ms
 * Brief: Funcion de retardo
 */

void delay_ms(unsigned int tiempo)
    {
    demora_tim1 = 0;
    while(demora_tim1 < tiempo );

    }
