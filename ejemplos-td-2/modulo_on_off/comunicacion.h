/*
 * comunicacion.h
 *
 *  Created on: 5/2/2015
 *      Author: Mart�n
 */

#ifndef COMUNICACION_H_
#define COMUNICACION_H_



/* ======== COMANDOS ========= */
#define RELE_ON		0x0F
#define	RELE_OFF	0x01
#define	RELE_TOGGLE	0xDA
#define ENTRO_PANTALLA	0x07

#define	LUCES_ON	0x1F
#define	LUCES_OFF	0x11
#define	LUCES_TOGGLE 0x1A
#define LUCES_DUTY	0x1D
#define LUCES_AUMENTAR 0x1B
#define	LUCES_DISMINUIR 0x1C

/* ======= FRECUENCIAS ======== */
#define FREC_LUCES	0x66
#define	FREC_ON_OFF	0x78

typedef enum {
	DUTY0 = 250,					// (250) -> 0%
	DUTY10 = 225 ,				// (225) -> 10%
	DUTY20 = 200,				// (200) -> 20%
	DUTY30 = 175,				// (175) -> 30%
	DUTY40 = 150,				// (150) -> 40%
	DUTY50 = 125,				// (125) -> 50%
	DUTY60 = 100,				// (100) -> 60%
	DUTY70 = 75,				// (75) -> 70%
	DUTY80 = 50,				// (50) -> 80%
	DUTY90 = 25,				// (25) -> 90%
	DUTY100 = 0,				// (0) -> 100%
}luces_duty_t;

#endif /* COMUNICACION_H_ */
