/*
 * main.c
 *
 *  Created on: 5/2/2015
 *      Author: Mart�n
 */
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "nrf24l01.h"
#include "mcu.h"
#include "hardware.h"
#include "comunicacion.h"

volatile unsigned char luces = 0, duty = 255, duty_ant = 250, cambiar_duty = 0, reportar = 0;
volatile unsigned char tmr0_ovf = 0;
uint8_t *data;

int main(void)
{
	init_megazord();

	while(1)
	{

		if (demora_tx >= 20)
		{
			reset();
			Modo_RX();
			CE = 1;
			demora_tx = 0;
		}
		if(reportar)
		{
			Modo_TX();

			data[1] = luces? LUCES_ON : LUCES_OFF;
			transmit_payload(data);
			reportar = 0;

		}
	}
	return 0;
}


ISR(INT0_vect)
{

	cli();
	CE = 0; // CE bajo

	data = WriteToNrf(R, R_RX_PAYLOAD, data, dataLen); //Leo el mensaje

	switch(data[1])
	{
		case LUCES_TOGGLE:
			if(luces)
			{
				LED = 1;
				duty_ant = duty;
				duty = 255;
				luces = 0;
			}
			else
			{
				LED = 0;
				duty = duty_ant;
				luces = 1;
			}
			// Probando solo con el led onboard
			LED ^= 1; //Toggle
			reportar = 1;
			break;
		case LUCES_AUMENTAR:
			if(duty <= 230)
				duty = duty + 25;
				duty_ant = duty;
			break;
		case LUCES_DISMINUIR:
			if(duty >= 25)
				duty = duty - 25;
				duty_ant = duty;
			break;
		default:
			break;

	}

	data[1] = 0;

	sei();
	CE = 1; // CE alto
}

ISR (TIMER0_OVF_vect)
{
	TCNT0 = 55; // Desborda cada 200us
	tmr0_ovf++;
	if(tmr0_ovf == 5)
	{
		//Pas� 1ms
		tmr0_ovf = 0;
		demora_tx++;
		tiempo_tx++;
	}
}

ISR (TIMER1_OVF_vect)
{
	OCR1A = duty;
}

