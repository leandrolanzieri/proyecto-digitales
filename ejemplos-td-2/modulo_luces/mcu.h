/*
 * mcu.h
 *
 *  Created on: 5/2/2015
 *      Author: Mart�n
 */

#ifndef MCU_H_
#define MCU_H_

extern volatile unsigned int demora_tim1;
extern volatile unsigned int demora_tx;
extern volatile unsigned int tiempo_tx;

void init_megazord(void);
void init_SPI(void);
void init_INT(void);
void init_SystemTick(void);
void init_PWM(void);

#endif /* MCU_H_ */
