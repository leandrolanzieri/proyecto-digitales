/*
 * mcu.c
 *
 *  Created on: 5/2/2015
 *      Author: Martín
 */
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "nrf24l01.h"
#include "mcu.h"
#include "hardware.h"

volatile unsigned int demora_tim1=0;
volatile unsigned int demora_tx=0;
volatile unsigned int tiempo_tx=0;

void init_megazord(void)
{
	LED_DDR |= (1 << LED_PIN); //LED SALIDA

	LUCES_DDR |= (1 << LUCES_PIN); //LUCES SALIDA

	init_SystemTick();
	init_PWM();
    init_SPI();
    nrf24L01_init();
    init_INT();
    sei();
}


void init_SystemTick(void)
{
	// Se utiliza el TMR0 como System Tick, con un desborde cada 200us
	TCNT0 = 55; // Desborda cada 200us
	TIMSK |= (1 << TOIE0); // Habilito la interrupción por desborde
	TCCR0 |= (1 << CS01) | (1 << CS00); // Prescaler de 8
}

void init_PWM(void)
{
	//Timer1 para el PWM de las luces
	TIMSK |= (1 << TOIE1); // Habilito la interrupción por desborde
	TCCR1A |= (1<<COM1A1) | (1<<WGM10); // Modo Fast PWM de 8 bits
	TCCR1B |= (1<<WGM12) | (1<<CS11);
}


void init_SPI(void)
{
	// Pongo SCK, MOSI, CSN y CE como salidas
	SCK_DDR |= (1 << SCK_PIN);

	MOSI_DDR |= (1 << MOSI_PIN);

	CE_DDR |= (1 << CE_PIN);

	CSN_DDR |= (1 << CSN_PIN);

	// Habilito SPI y pongo el micro como master
	SPCR |= (1 << SPE) | (1 << MSTR) | (1 << SPR0);

	CSN = 1;
	CE = 1;
}


void init_INT(void)
{
	DDRD &= ~(1 << DDD2);		// INT0 (PD2) como entradas

	MCUCR |= (1 << ISC01); 		// INT0 e INT1 en falling edge
	MCUCR &= ~(1 << ISC00);

	GICR |= (1 << INT0); 		//Habilito la interrupción
}

