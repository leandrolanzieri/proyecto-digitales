# Proyecto de Domótica
### Martín Enríquez - Leandro Lanzieri
#### Proyecto realizado para la cátedra de Técnicas Digitales III - UTN FRA

## Objetivo Principal
Se pretende armar un sistema de control automatizado y reprogramable para hogares, de bajo costo, inalámbrico y accesible de manera remota a través de Internet. La idea es poder establecer una red formada por nodos sensores y actuadores que reporten a uno central donde se toman decisiones en consecuencia. El nodo central también es responsable de interactuar con el usuario de manera local y a través de Internet en caso de poseer conexión.

[Más información en la Wiki](https://gitlab.com/leandrolanzieri/proyecto-digitales/wikis/home)
===
Copyright (c) 2015  Martin Enriquez, Leandro Lanzieri Rodriguez

**Proyecto Domótica** se encuentra bajo la licencia [GNU GPLv3](http://www.gnu.org/licenses/gpl-3.0.en.html).